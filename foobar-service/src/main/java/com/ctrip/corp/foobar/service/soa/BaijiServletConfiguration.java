package com.ctrip.corp.foobar.service.soa;

import org.springframework.context.annotation.Configuration;

import com.ctriposs.baiji.rpc.extensions.springboot.BaijiRegistrationBean;

@Configuration
public class BaijiServletConfiguration extends BaijiRegistrationBean {

	protected BaijiServletConfiguration() {
		super("/api/*", HelloSOAServiceImpl.class);
	}

}