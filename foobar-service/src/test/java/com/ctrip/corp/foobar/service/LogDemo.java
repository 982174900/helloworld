package com.ctrip.corp.foobar.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import com.ctrip.framework.triplog.shaded.client.tag.TagMarkerBuilder;

import java.util.concurrent.TimeUnit;

/**
 * Created by zhuozhang on 2016/7/22.
 */
public class LogDemo {

    private static final Logger log = LoggerFactory.getLogger(LogDemo.class);

    public static void main(String[] args) throws Exception {
        Marker marker = TagMarkerBuilder.newBuilder().add("tagA", "valueA").add("tagB", "valueB").build();
        log.info(marker, "Hello World");
        TimeUnit.SECONDS.sleep(10L);
    }
}
