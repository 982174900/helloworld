# 操作步骤 
* 将项目以maven项目导入IDE
* `Dubbo Service`项目可以通过src/test/java/com.ctrip.corp.foobar.cdubbo/ServiceStarter.java启动，进行本地开发调试
* `Soa Service`项目可以通过src/test/java/com.ctrip.corp.foobar.service/ServiceStarter.java启动，进行本地开发调试
* `SOAP`项目可以通过src/test/java/com.ctrip.corp.foobar.soap/ServiceStarter.java启动，进行本地开发调试
* `Web`项目可以通过src/test/java/com.ctrip.corp.foobar/WebStarter.java启动，进行本地开发调试
* `Job`项目可以通过src/test/java/com.ctrip.corp.foobar/JobStarter.java启动，进行本地开发调试

# 说明
* 每个子项目中都提供了README.md文件，具体细节请参考子项目中的READ.md