package com.ctrip.corp.foobar.web;

import java.awt.Desktop;
import java.net.URI;

import org.springframework.boot.SpringApplication;

public class WebStarter {

  public static void main(String[] args) throws Exception {
    System.setProperty("java.awt.headless", "false");
    // 确保先启动服务端程序  本地模式请参考 http://conf.ctripcorp.com/pages/viewpage.action?pageId=192829007
    System.setProperty("artemis.client.local.enabled", "true");

    SpringApplication.run(WebInitializer.class);

    // port 8080 is configured in src/test/resources/application.properties(key: server.port)
    Desktop.getDesktop().browse(new URI("http://127.0.0.1:8080"));
  }
}
